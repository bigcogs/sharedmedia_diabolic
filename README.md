[ ![Support me on Patreon](http://i.imgur.com/kVU2d3f.png) ](https://www.patreon.com/cogwerkz) 

Various statusbars, fonts, borders, background and other textures to match DiabolicUI or any other Diablo themed user interface. Supports all addons that support SharedMedia.

## Join us on social media  

* Discord: [Mqnrr8t](https://discord.gg/Mqnrr8t)
* Facebook: [@cogwerkz](https://www.facebook.com/cogwerkz)
* Twitter: [@CogwerkzUI](https://twitter.com/CogwerkzUI)

## Buy me a cup of coffee  
Pledge to my work on Patreon and get cool behind the scenes posts and source materials, or buy me a cup of coffee with PayPal and keep me awake while working!  

* PayPal: [www.paypal.me/larsnorberg](https://www.paypal.me/larsnorberg)
* Patreon: [www.patreon.com/cogwerkz](https://www.patreon.com/cogwerkz) 

## Report bugs or request features  
Bug reports and feature suggestions are all done through BitBucket's Issue tracker linked below. Remember to check existing tickets before submitting any reports, as chances are your question has already been answered.   

* Issues: [bitbucket.org/bigcogs/sharedmedia_diabolic/issues](https://bitbucket.org/bigcogs/sharedmedia_diabolic/issues)

Regards  
Lars *"Goldpaw"* Norberg