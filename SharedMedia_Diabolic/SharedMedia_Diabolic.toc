## Interface: 70300
## Name: SharedMedia_Diabolic
## Title: SharedMedia: |cff8a0707Diabolic|r
## Version: 1.0
## Author: Lars "Goldpaw" Norberg
## Notes: Various media to match |cff8a0707Diabolic|r|cffffffffUI|r or any other Diablo themed user interface.|n|n|cff4488ffPayPal|r|cffffffff:|r |n|cffffffffpaypal@cogwerkz.org|r |n|n|cff4488ffPayPal.Me|r|cffffffff:|r|n|cffffffffwww.paypal.me/larsnorberg|r|n|n|cff4488ffPatreon|r|cffffffff:|r |n|cffffffffwww.patreon.com/cogwerkz|r
## DefaultState: Enabled
## OptionalDeps: LibSharedMedia-3.0
## X-Website: http://www.cogwerkz.org
## X-Donate: PayPal:paypal@cogwerkz.org
## X-License: The MIT License (MIT)
## X-Category: Artwork

libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
embeds.xml
main.lua
