local ADDON = ...

local LSM = LibStub and LibStub("LibSharedMedia-3.0")
if (not LSM) then 
	return 
end 

local getPath = function(lib, file)
	return ([[Interface\AddOns\%s\media\%s\%s]]):format(ADDON, lib, file)
end

-- backgrounds & general artwork
LSM:Register("background", "Diabolic Angel", getPath("background", "DiabolicUI_Artwork_Angel.tga"))
LSM:Register("background", "Diabolic Demon", getPath("background", "DiabolicUI_Artwork_Demon.tga"))
LSM:Register("background", "Diabolic Skull", getPath("background", "DiabolicUI_Artwork_Skull.tga"))
LSM:Register("background", "Diabolic Minimap Border Large", getPath("background", "DiabolicUI_Minimap_CircularBorder.tga"))
LSM:Register("background", "Diabolic Minimap Border Spiky", getPath("background", "DiabolicUI_Minimap_160x160_Border.tga"))
LSM:Register("background", "Diabolic Badge Backdrop", getPath("background", "DiabolicUI_MinimapIcon_Circular.tga"))
LSM:Register("background", "Diabolic Globe Backdrop", getPath("background", "DiabolicUI_PlayerGlobes_150x150_Backdrop.tga"))
LSM:Register("background", "Diabolic Globe Border", getPath("background", "DiabolicUI_PlayerGlobes_150x150_Border.tga"))
LSM:Register("background", "Diabolic Tooltip Backdrop", getPath("background", "DiabolicUI_Tooltip_Background.tga"))
LSM:Register("background", "Diabolic StatusBar Dark Backdrop", getPath("background", "DiabolicUI_StatusBar_512x64_Backdrop_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Medium Backdrop ", getPath("background", "DiabolicUI_StatusBar_128x16_Backdrop_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Medium Gloss", getPath("background", "DiabolicUI_StatusBar_128x16_Overlay_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Medium Glow", getPath("background", "DiabolicUI_StatusBar_128x16_Glow_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Medium Threat", getPath("background", "DiabolicUI_StatusBar_128x16_Threat_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Plate Backdrop", getPath("background", "DiabolicUI_StatusBar_64x8_Backdrop_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Plate Glow", getPath("background", "DiabolicUI_StatusBar_64x8_Glow_Warcraft.tga"))
LSM:Register("background", "Diabolic StatusBar Plate Threat", getPath("background", "DiabolicUI_StatusBar_64x8_Threat_Warcraft.tga"))

-- borders
LSM:Register("border", "Diabolic Glow", getPath("border", "DiabolicUI_GlowBorder_128x16.tga"))
LSM:Register("border", "Diabolic Thick", getPath("border", "DiabolicUI_Tooltip_Border.tga"))
LSM:Register("border", "Diabolic Thin", getPath("border", "DiabolicUI_Tooltip_Small.tga"))
LSM:Register("border", "Diabolic TooltipBody", getPath("border", "DiabolicUI_Tooltip_Body.tga"))
LSM:Register("border", "Diabolic TooltipFooter", getPath("border", "DiabolicUI_Tooltip_Footer.tga"))
LSM:Register("border", "Diabolic TooltipHeader", getPath("border", "DiabolicUI_Tooltip_Header.tga"))

-- statusbars
LSM:Register("statusbar", "Diabolic Dark", getPath("statusbar", "DiabolicUI_StatusBar_512x64_Dark_Warcraft.tga"))
LSM:Register("statusbar", "Diabolic Medium", getPath("statusbar", "DiabolicUI_StatusBar_128x16_Normal_Warcraft.tga"))
LSM:Register("statusbar", "Diabolic Plate", getPath("statusbar", "DiabolicUI_StatusBar_64x8_Normal_Warcraft.tga"))

-- fonts
LSM:Register("font", "NotoSans-Bold", getPath("fonts", "NotoSans-Bold.ttf"))
LSM:Register("font", "NotoSans-Regular", getPath("fonts", "NotoSans-Regular.ttf"))
LSM:Register("font", "NotoSerif-Bold", getPath("fonts", "NotoSerif-Bold.ttf"))
LSM:Register("font", "NotoSerif-Regular", getPath("fonts", "NotoSerif-Regular.ttf"))
LSM:Register("font", "ExocetBlizzardLight", getPath("fonts", "ExocetBlizzardLight.ttf"))
LSM:Register("font", "ExocetBlizzardMedium", getPath("fonts", "ExocetBlizzardMedium.ttf"))
LSM:Register("font", "Coalition", getPath("fonts", "Coalition.ttf"))
